<p align="center">
  <img width = "200px" src="https://suraj.codes/dist/images/chrome_Uv5c1kbpE4.png" alt="Logo"/>
</p>

### Current tasks
- Currently working on developing a decent selfhosted continuous integration service

## Plan

Cross platform app which can be used to store information such as your daily log of activities you've completed and tasks - activites you need to complete.

Astra will be modular, servers will have channels, these channels will follow different datatypes and can be embedded or linked within other channels. Each server can have as many or as few channels as the user wants. The "ALL" category will aggregate information from all the servers and display the information with tags text to them. Servers can be renamed, given icons and different colours to differentiate them. Categories will also be implemented, these will allow the user to organise the server if there are lots of channels. Channels such as the Code Journals, Mood boards and Note books can have multiple of the same afforementioned content type in any one given channel. For example, you can have multiple Note Book cards inside a channel with the content type Note Book.


    |                    |                    |                               |
    |    WORKSPACES      |      CHANNELS      |         CAPTAINS LOG          |
    |____________________|____________________|_______________________________|
    | > EXAMPLE SERVER 1 | > CAPTAINS LOG     |======<    17th June    >======|
    | EXAMPLE SERVER 2   | TASKS              | 💡 - Wrote Astra README       |
    | EXAMPLE SERVER 3   | NOTE BOOK          | 💡 - Finished app case study  |
    | EXAMPLE SERVER 4   | MOOD BOARD         |                               |
    |                    | CODE JOURNAL       |                               |
    | ALL                |                    |                               |


### Content types
The plan for content types is still being fine tuned however the current content types are as follows:

| Type  | Description |
| ------------- | ------------- |
| **Default** Captain Log  | The Captains Log can be used to keep a log of everything you do on a day to day basis, this essentially acts as a diary so the user can keep track of what they've done  |
| **Default** Tasks  | The tasks section can be used to log what you need to complete, these can then be ticked off, once ticked off they will be added to the Captain's Log with an additional comment on the date they were completed |
| Notebook | The Notebook will be Markdown enabled and will allow the user to keep notes relating to what they want
| Mood Board | The Mood Board would be useful for designers and can be used to keep images (with an associated link if applicable) as inspiration
| Code Journal | The Code Journal would be useful for developers and can be used to keep a log of code snippets, for instance array sorting in JavaScript or useful Bash commands

#### External content types
External content types may be added in the future, these will be placed under a seperate channel with the tag "Feeds". These will include; RSS feeds and Feeds direct from Reddit subreddits.

### Extendability
Due to the modular structure adding new content types should be relatively easy. A .astra file extension will be required and once dragged on will load in the content type plugin.

#### Dot Astra files
.astra files will follow a set template similar similar to YAML preprocessing. .astra files can either be content type plugins using JavaScript or themes using css. A name, author, version and type (plugin or theme) will need to be specified at the head of the file with the  body containing either JavaScript or CSS.

#### In app installation utilities
An in app plugin and theme picker will be integrated so users can remove or disable different plugins and themes. Users will also be able to enter the URL to a raw .astra file to load it. Once beta is released a store could be released which could show a list of installable plugins and themes

### Teams & Sharing
Teams will be implemented, this will allow multiple people to work within one server, a robust permissions type system will need to be implemented and roles can be assigned. Individual roles can be created or member permissions editted for different channels ranging read/write access to just read or not access at all. Roles with permissions such as "Administrator" will bypass channel specific permissions. People with the administrator role will also be able to add or remove team members.

Link sharing will allow users to copy a link for certain components inside different channels, for instance they may copy a link to multiple/individual code snippets as well as Note Book entries, Mood Board collections, Tasks and the Captains Log. The links will not require the app to be installed on the recipitents computer, however there would be limited functionality - such as the inability to edit such information from the web based client.

### Cloud saves
Astra will support cloud saving, this will allow your information to sync between different devices. You can opt in to have your data stored locally however you will not be able to share your workspace or links for individual components. Due to the nature of cloud saving, the user will require an account verified by Email.

### Widget based control system
Astra should support split screen capabilities, meaning the user should be able to drag one channel to the right/left of the screen and the component should split and take up the afforementioned segment of the screen - not desimilar to that of the functionality found on text editors such as VSCode and Atom

### Copying and pasting
Astra will allow users to copy and paste components such as notebook entries and component logs. Users will also be able to export this data to JSON and CSV format as well as a .astradata file which can be dragged onto Astra to load the given channel/component into a workspace/channel

### Integration
I plan on creating a CLI to interface with Astra, through the CLI people will be able to add or remove items from the captains log as well as view there notes. This will allow the user to specifically note down logs while they work in the CLI. A Git tag may also be added in the captains log, meaning Astra can be integrated in the Git workflow to log commit data. A protocol handler will also be setup to open Astra from Chrome and other apps.

### Usability
Astra should make it as easy as possible to access on the go, the initial plan is to have a stand alone app due to the required implementation of protocols and secure data storage without accounts. A tray or status bar icon and widget may be created for quick access as well as a web interface which directly communicates with the local app, this could be embedded as a new tab page

## HCI Sources
https://theascent.pub/the-5-benefits-of-keeping-a-daily-log-8d90eba47a05

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
