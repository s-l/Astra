import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

// Modules
import modalStore from "./modules/modalStore";
import mainNavbarStore from "./modules/mainNavbarStore";

export default new Vuex.Store({
  modules: {
    modalStore: modalStore,
    mainNavbarStore: mainNavbarStore
  }
});
