let mainNavbarStore = {
    state: {
        navVisibilityIndex: {
            mainNavbar: false,
            workspaceNavbar: true
        }
    }
};

export default mainNavbarStore;
  