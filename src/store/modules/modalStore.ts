interface payloadArguments {
  override: boolean;
}

interface modalStore {
  addWorkspaceCard: {
    visible: boolean
  }
}

let modalStore = {
  state: {
    addWorkspaceCard: {
      visible: false
    }
  },
  mutations: {
    toggleVisibility(state: modalStore, payload: payloadArguments) {
      state.addWorkspaceCard.visible =
        payload.override || !state.addWorkspaceCard.visible;
      return state.addWorkspaceCard.visible;
    }
  },
  actions: {}
};

export default modalStore;
