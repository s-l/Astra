// Vue internals
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/storeBootstrapper";
import "./registerServiceWorker";

// OSS
import anime from "animejs";

// Config bootstrapping
Vue.config.productionTip = false;

// Global Vue instance setup
Vue.prototype.$animate = anime;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
