## Default structure

astraAppContainer - Top level component for all child elements. Will also contain overlays, modals and the status bar
channels - Channels which corrospond to the icons in the navbar, workspaces, notifications, orientation and support
fullscreenContainer - Bootstraps the sidebar and all child elements
