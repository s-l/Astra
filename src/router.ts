import Vue from "vue";
import Router from "vue-router";
import astraAppContainer from "./views/public/astraAppContainer/astraAppContainer.vue";

// Channel imports
import astraWorkspaceSelector from "./views/public/channels/astraWorkspaceSelector/selector.vue";

// Workspaces import
import createdWorkspaceView from "./views/public/channels/createdWorkspace/createdWorkspaceView.vue";


Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "mainPage",
      component: astraAppContainer,
      children: [
        {
          path: "/workspace",
          name: "astraWorkspace",
          component: astraWorkspaceSelector,
        },
        {
          path: "/select",
          name: "astraWorkspaceSelector",
          component: createdWorkspaceView
        }
      ]
    }
  ]
});
